<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
    $this->load->library('session');
		$this->load->model('medici_model');
	}

	private function check_login($next)
	{
		if (!$this->session->userdata('logged_in'))
		{
			redirect('controller/login?next='.$next, 'refresh');
		}
	}

	public function login()
	{
		//GET
		$next = $this->input->get('next');

		if($this->input->method() !== 'post')
	    {
				$this->load->view('base/head');
				$this->load->view('login/head');
				$this->load->view('base/header');
				$this->load->view('login/header');
				$this->load->view('login/content');
				$this->load->view('base/footer');
      }
      else
      {
        	$user = $this->medici_model->can_login($this->input->post('cf'), $this->input->post('password'));
					$this->session->set_userdata('user', $user);

					if ($user !== NULL)//SUCCESS LOGIN
					{
						$this->session->set_userdata('logged_in', TRUE);

						//GET DEFAULT NEXT
						if($user->is_medico)
						{
							$defaultNext = 'medici/index';
						}
						elseif ($user->is_paziente)
						{
							$defaultNext = 'pazienti/index';
						}
						elseif ($user->is_segretario)
						{
							$defaultNext = 'segretari/index';
						}
						elseif ($user->is_amministratore)
						{
							$defaultNext = 'amministratori/index';
						}
						else
						{
							$defaultNext = 'controller/index';
						}

						if($next !== NULL)
						{
							if(strpos($next, 'medici') !== 0 && !$user->is_medico)
							{
								$next = $defaultNext;
							}
							elseif (strpos($next, 'pazienti') !== 0 && !$user->is_paziente)
							{
								$next = $defaultNext;
							}
							elseif (strpos($next, 'segretari') !== 0 && !$user->is_segretario)
							{
								$next = $defaultNext;
							}
							elseif (strpos($next, 'amministratori') !== 0 && !$user->is_amministratore)
							{
								$next = $defaultNext;
							}
						}
						else
						{
							$next = $defaultNext;
						}
						redirect($next, 'refresh');
					}
					else
					{
						$this->session->set_userdata('logged_in', FALSE);
						$this->load->view('base/head');
						$this->load->view('login/head');
						$this->load->view('login/header');
						$this->load->view('login/content', array('errore' => TRUE));
						$this->load->view('base/footer');
					}
	    }
	}

	public function logout()
	{
		$this->session->set_userdata('logged_in', FALSE);
		redirect('controller/index', 'refresh');
	}


	//***************************************************************************
	//PAGINE

	public function index()
	{
		$this->load->view('base/head');
			$this->load->view('index/head');
		$this->load->view('base/header');
			$this->load->view('index/content');
		$this->load->view('base/footer');
	}
	
	public function registrazione()
	{
		if($this->input->method() !== 'post')
		{		
			$medici = $this->medici_model->get_medici();
			$data['medici'] = $medici;
			
			$this->load->view('base/head');
				$this->load->view('registrazione/head');
			$this->load->view('base/header');
				$this->load->view('registrazione/content', $data);
			$this->load->view('base/footer');
		}
		else
		{
			$this->medici_model->registra_utente(	$this->input->post('nome'), 
													$this->input->post('cognome'), 
													$this->input->post('email'), 
													$this->input->post('cf'), 
													$this->input->post('residenza'), 
													$this->input->post('medici'), 
													$this->input->post('ruolo'), 
													$this->input->post('password'), 
													$this->input->post('sesso'));				
			$this->load->view('base/head');
				$this->load->view('index/head');
			$this->load->view('base/header');
				$this->load->view('index/content');
			$this->load->view('base/footer');													
		}
	}

/*	public function test()
	{
		$this->check_login('controller/test');
		$data['objects'] = $this->medici_model->get_famiglie();  // array di oggetti
		$this->load->view('base/head');
		$this->load->view('test/head');
		$this->load->view('base/header');
		$this->load->view('test/content', $data);
		$this->load->view('base/footer');
	}


	public function persone()
	{
		$this->check_login('controller/persone');
		$data['objects'] = $this->medici_model->get_persone();  // array di oggetti
		$this->load->view('base/head');
		$this->load->view('pazienti/head');
		$this->load->view('base/header');
		$this->load->view('pazienti/content', $data);
		$this->load->view('base/footer');
	}

	public function medici()
	{
		$this->check_login('controller/medici');
		$data['objects'] = $this->medici_model->get_studi_medici();  // array di oggetti
		$this->load->view('base/head');
		$this->load->view('pazienti/head');
		$this->load->view('base/header');
		$this->load->view('pazienti/content', $data);
		$this->load->view('base/footer');
	}

	public function famiglie()
	{
		$this->check_login('controller/famiglie');
		$data['objects'] = $this->medici_model->get_famiglie();  // array di oggetti
		$this->load->view('base/head');
		$this->load->view('pazienti/head');
		$this->load->view('base/header');
		$this->load->view('pazienti/content', $data);
		$this->load->view('base/footer');
	}*/

}
