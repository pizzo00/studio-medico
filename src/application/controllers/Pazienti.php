<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pazienti extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
    $this->load->library('session');
		$this->load->model('medici_model');
	}

	private function check_login($next)
	{
		if (!$this->session->userdata('logged_in') || !$this->session->userdata('user')->is_paziente)
		{
			redirect('controller/login?next='.$next, 'refresh');
		}
	}

	//***************************************************************************
	//PAGINE

	public function index()
	{
		$this->check_login('pazienti/index');

		$this->load->view('base/head');
		$this->load->view('pazienti/index/head');
		$this->load->view('pazienti/base/header');
		$this->load->view('pazienti/index/content');
		$this->load->view('base/footer');
	}

	public function farmaci()
	{
		$this->check_login('pazienti/farmaci');

		$this->load->view('base/head');
		$this->load->view('pazienti/farmaci/head');
		$this->load->view('pazienti/base/header');
		$this->load->view('pazienti/farmaci/content');
		$this->load->view('base/footer');
	}
}
