<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
    $this->load->library('session');
		$this->load->model('medici_model');
	}

	private function check_login($next)
	{
		if (!$this->session->userdata('logged_in'))
		{
			redirect('controller/login?next='.$next, 'refresh');
		}
	}

	public function farmaci()
	{
		$this->check_login('api/farmaci');

		//GET
		$search = $this->input->get('search') ?: "";
		$search = "%".$search."%";
		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');

		//SESSION
		$user = $this->session->userdata('user');

		$result = $this->medici_model->get_farmaci($user->id, $search);

		$total = sizeof($result);
		$rows = array_slice($result, $offset, $limit);

		$data = (object) [
			'total' =>  $total,
			'rows' => $rows];

		$this->output->set_header("Content-Type: text/plain");
		printf(json_encode($data));
	}

	public function farmaci_from_medico()
	{
		$this->check_login('api/farmaci');

		//GET
		$search = $this->input->get('search') ?: "";
		$search = "%".$search."%";
		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');
		$user = $this->input->get('paziente');

		$result = $this->medici_model->get_farmaci($user, $search);

		$total = sizeof($result);
		$rows = array_slice($result, $offset, $limit);

		$data = (object) [
			'total' =>  $total,
			'rows' => $rows];

		$this->output->set_header("Content-Type: text/plain");
		printf(json_encode($data));
	}


	public function pazienti_medico()
	{
		$this->check_login('api/farmaci');

		//GET
		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');

		//SESSION
		$user = $this->session->userdata('user');

		$result = $this->medici_model->get_pazienti_medico($user->id);

		$total = sizeof($result);
		$rows = array_slice($result, $offset, $limit);

		$data = (object) [
			'total' =>  $total,
			'rows' => $rows];

		$this->output->set_header("Content-Type: text/plain");
		printf(json_encode($data));
	}
	public function cancella_farmaco()
	{
		$id_persona = $this->input->get('id_persona');
		$id_farmaco = $this->input->get('id_farmaco');
		$this->medici_model->delete_farmaco($id_persona, $id_farmaco);
	}

	public function aggiungi_farmaco()
	{
		$id_persona = $this->input->get('id_persona');
		$id_farmaco = $this->input->get('id_farmaco');
		$this->medici_model->insert_farmaco($id_persona, $id_farmaco);
	}

}
