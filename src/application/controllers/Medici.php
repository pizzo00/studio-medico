<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medici extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
    $this->load->library('session');
		$this->load->model('medici_model');
	}

	private function check_login($next)
	{
		if (!$this->session->userdata('logged_in') || !$this->session->userdata('user')->is_medico)
		{
			redirect('controller/login?next='.$next, 'refresh');
		}
	}

	//***************************************************************************
	//PAGINE

	public function index()
	{
		$this->check_login('medici/index');

		$this->load->view('base/head');
		$this->load->view('medici/index/head');
		$this->load->view('medici/base/header');
		$this->load->view('medici/index/content');
		$this->load->view('base/footer');
	}

	public function farmaci()
	{
		$this->check_login('medici/farmaci');

		$id = $this->session->userdata('user')->id;
		$pazienti = $this->medici_model->get_pazienti_medico($id);
		$farmaci = $this->medici_model->get_all_farmaci();

		$this->load->view('base/head');
		$this->load->view('medici/farmaci/head');
		$this->load->view('medici/base/header');
		$this->load->view('medici/farmaci/content', array('pazienti' => $pazienti, 'farmaci' => $farmaci));
		$this->load->view('base/footer');
	}
}
