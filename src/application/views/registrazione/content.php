<div class="container">
  <form action="#" method="post" id="signup-form" class="form-signup">
	  <div class="text-center"><h1 class="text-danger">Studio Medico</h1></div>
	  <h4 class="mb-3 mt-3 ml-1 font-weight-normal" style="text-align: center;">Registrazione</h4>
	  <input type="text" name="nome" autofocus="" maxlength="254" required="" id="nome" class="my-form-control form-control" placeholder="Nome">
	  <input type="text" name="cognome" autofocus="" maxlength="254" required="" id="cognome" class="my-form-control form-control" placeholder="Cognome">
	  
	  <p>
		<br>
		<input type="radio" name="sesso" required="" value="M"> Maschio
		<input type="radio" name="sesso" required="" value="F"> Femmina<br>
	  </p>
	  <input type="text" name="email" autofocus="" maxlength="254" required="" id="email" class="my-form-control form-control" placeholder="E-mail">
	  <input type="text" name="cf" autofocus="" maxlength="254" required="" id="cf" class="my-form-control form-control" placeholder="Codice Fiscale">
	  <input type="text" name="residenza" autofocus="" maxlength="254" required="" id="residenza" class="my-form-control form-control" placeholder="Paese di residenza">	  

	  
	  <select name="medici" required=""  class="my-form-control form-control" placeholder="Seleziona il tuo medico">
		<?php foreach($medici as $m): ?>
			<option value = "<?=$m->id?>"><?=$m->cognome?> <?=$m->nome?></option>
		<?php endforeach ?>
	  </select>
	  
	  <input list="ruolo" name="ruolo" maxlength="254" required=""  class="my-form-control form-control" placeholder="Ruolo">
	  <datalist id="ruolo">
		<option value="Paziente">
		<option value="Medico">
		<option value="Segretario">
		<option value="Amministratore">		
	  </datalist>
	  
	  <input type="password" name="password" required="" id="password" class="my-form-control form-control" placeholder="Password">
	  <button type="submit" class="btn btn-lg btn-danger btn-block">Registrati</button>
</form>
</div>
