<div class="container">
  <form action="#" method="post" id="login-form" class="form-signin">
  <div class="text-center"><h1 class="text-danger">Studio Medico</h1></div>
  <h4 class="mb-3 mt-3 ml-1 font-weight-normal" style="text-align: center;">Login</h4>
  <input type="text" name="cf" autofocus="" maxlength="254" required="" id="cf" class="my-form-control form-control" placeholder="Codice Fiscale">
  <input type="password" name="password" required="" id="password" class="my-form-control form-control" placeholder="Password">
  <button type="submit" class="btn btn-lg btn-danger btn-block">Login</button>
</form>
</div>
