<div class="container">
  <h1>Benvenuti nello studio medico Rossi</h1>
  <row class="btn-group">
    <a class="btn btn-lg text-light btn-danger" href="<?php echo site_url('controller/login') ?>">Accedi</a>
    <a class="btn btn-lg text-light btn-secondary" href="<?php echo site_url('controller/registrazione') ?>">Registrati</a>
  </row>
</div>
