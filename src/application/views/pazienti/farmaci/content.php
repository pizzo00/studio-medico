<div class="container">
  <h1>Farmaci</h1>
  <div class="row">
    <div class="col">
      <table id="table"
            data-search="true"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, ALL]"
            data-side-pagination="server"
            data-url="<?php echo site_url('api/farmaci') ?>"
            data-locale="it-it">
      </table>
    </div>
  </div>
</div>

<script>
"use strict";
    var $table = $('#table');

    function initTable() {
        $table.bootstrapTable({
            //height: getHeight(),
            columns:
                [{
                    field: 'state',
                    checkbox: true,
                    rowspan: 1,
                    align: 'center',
                    valign: 'middle'
                },
                {
                    title: 'Nome',
                    field: 'nome',
                    align: 'center',
                    valign: 'middle',
                    sortable: false
                }]
        });
        // sometimes footer render error.
        setTimeout(function () {
            $table.bootstrapTable('resetView');
        }, 200);

        $(window).resize(function () {
            $table.bootstrapTable('resetView', {
                height: getHeight()
            });
        });
    }

    /*-----------------------/
    /-----TABLE FUNCTION-----/
    /-----------------------*/
    function getIdSelections_MM() {
        return $.map($table.bootstrapTable('getSelections'), function (row)
        {
            return row.id
        });
    }
    function getSelections_MM() {
        return $table.bootstrapTable('getSelections');
    }
    function getHeight() {
        return $(window).height() - $('h1').outerHeight(true);
    }

    $(function () {
        initTable();
    });
</script>
