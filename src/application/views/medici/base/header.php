<title>Studio Medico</title>
</head>
<body>
<header>
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-danger shadow">
    <a class="navbar-brand" href="#">Studio Medico</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav mr-auto">
        <?php
          $page = $this->router->fetch_method();
        ?>
        <li class="nav-item <?php if($page == "index") { echo "active"; } ?>">
          <a class="nav-link" href="<?php echo site_url('medici/index') ?>">Home</a>
        </li>
        <li class="nav-item <?php if($page == "farmaci") { echo "active"; } ?>">
          <a class="nav-link" href="<?php echo site_url('medici/farmaci') ?>">Farmaci</a>
        </li>
      </ul>

      <ul class="navbar-nav navbar-right">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" style="color: #fff;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <b><?php
                $user = $this->session->userdata('user');
                echo $user->cognome.' '.$user->nome;
              ?></b>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="<?php echo site_url('controller/logout') ?>">Log out</a>
            </div>
          </li>
        </ul>
    </div>
  </nav>
</header>
