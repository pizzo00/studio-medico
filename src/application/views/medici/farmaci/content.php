<div class="container">
  <h1>Farmaci</h1>
  <div class="row col">
    <div class="form-inline">
      <h6>Seleziona Paziente</h6><div style="width: 10px"></div>
      <select id="paziente" class="form-control-sm">
        <?php
          foreach ($pazienti as $key => $o)
          {
              echo '<option value="'.$o->id.'">'.$o->cognome.' '.$o->nome.'</option>';
          }
        ?>
      </select>
    </div>
  </div>
  <hr>
  <div class="row col">
    <div class="form-inline">
      <h6>Aggiungi Farmaco</h6><div style="width: 10px"></div>
      <select id="farmaco" class="form-control-sm">
        <?php
          foreach ($farmaci as $key => $o)
          {
              echo '<option value="'.$o->id.'">'.$o->nome.'</option>';
          }
        ?>
      </select>
      <div style="width: 10px"></div>
      <button class="btn btn-sm btn-success" type="button" id="insert_farmaco"><i class="fas fa-plus fa-fw"></i></button>
    </div>
  </div>
  <div class="row col">
    <div class="col">
      <table id="table"
            data-search="true"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, ALL]"
            data-side-pagination="server"
            data-query-params="queryParams"
            data-url="<?php echo site_url('api/farmaci_from_medico') ?>"
            data-locale="it-it">
      </table>
    </div>
  </div>
</div>

<script>
"use strict";
    var $table = $('#table'),
        $select_paziente = $('#paziente'),
        $select_farmaco = $('#farmaco'),
        $insert_farmaco = $('#insert_farmaco');

    function initTable() {
        $table.bootstrapTable({
            //height: 300,
            columns:
                [{
                    title: 'Nome',
                    field: 'nome',
                    align: 'center',
                    valign: 'middle',
                    sortable: false
                },
                {
                    field: 'modalButton',
                    align: 'center',
                    valign: 'middle',
                    events: 'modalButtonClicked',
                    formatter: 'modalButtonFormatter',
                }]
        });
        // sometimes footer render error.
        setTimeout(function () {
            $table.bootstrapTable('resetView');
        }, 200);
    }

    function queryParams(params)
    {
      params.paziente = $select_paziente.val();
      return params
    }

    $select_paziente.on('change', function()
    {
      $table.bootstrapTable('refresh', {silent: false});
    });

    function modalButtonFormatter(value, row, index)
    {
        var output = '<button class="btn btn-sm btn-danger modalButton" type="button"><i class="fas fa-times fa-fw"></i></button>'
        return output;
    }
    var modalButtonClicked =
    {
        'click .modalButton': function (e, value, row, index)
        {
          var request =
          {
              url: '<?php echo site_url('api/cancella_farmaco') ?>',
              type: 'GET',
              data: '&id_persona=' + $select_paziente.val() + '&id_farmaco=' + row.id,
              success: function(res)
              {
                $table.bootstrapTable('refresh', {silent: false});
              }
          };
          $.ajax(request);
        }
    }

    $insert_farmaco.click( function()
    {
      $.ajax({
          url: '<?php echo site_url('api/aggiungi_farmaco') ?>',
          type: 'GET',
          data: '&id_persona=' + $select_paziente.val() + '&id_farmaco=' + $select_farmaco.val(),
          success: function(res)
          {
            $table.bootstrapTable('refresh', {silent: false});
          }
      });
    });



    /*-----------------------/
    /-----TABLE FUNCTION-----/
    /-----------------------*/
    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row)
        {
            return row.id
        });
    }
    function getSelections() {
        return $table.bootstrapTable('getSelections');
    }

    $(function () {
        initTable();
    });
</script>
