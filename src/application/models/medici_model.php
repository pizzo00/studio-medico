<?php
class medici_model extends CI_Model
{
  public function can_login($codice_fiscale, $password)
  {
    $this->db->where('codice_fiscale', $codice_fiscale);
    $this->db->where('password', md5($password));
    $query = $this->db->get('persone');
    //SELECT * FROM persone WHERE codice_fiscale = '$codice_fiscale' AND password = '$password'
  	if ($query->num_rows() == 1)
  	{
  		return $query->result()[0];
  	}
  	else
  	{
  		return NULL;
  	}
  }

  public function get_persone()
  {
    $query = $this->db->query("SELECT *
      FROM persone
      ORDER BY id");
  	return $query->result();  // restituisce un array di oggetti
  }

  public function get_studi_medici()
  {
    $query = $this->db->query("SELECT *
      FROM studi_medici
      ORDER BY sede");
  	return $query->result();  // restituisce un array di oggetti
  }

  public function get_famiglie()
  {
    $query = $this->db->query("SELECT *
      FROM famiglie
      ORDER BY cognome");
  	return $query->result();  // restituisce un array di oggetti
  }

  public function get_all_farmaci()
  {
    $query = $this->db->query("SELECT id, nome FROM farmaci");
    return $query->result();
  }

  public function get_farmaci($id, $search)
  {
    $query = $this->db->query("SELECT f.id, f.nome
  							FROM farmaci AS f
  							INNER JOIN persone_has_farmaci AS p on f.id = p.farmaci_id
  							WHERE persone_id = ?
  							AND f.nome LIKE ?", array ($id, $search));
    return $query->result();
  }

  public function get_pazienti_medico($id)
  {
	  $query = $this->db->query("SELECT pazienti.id, pazienti.nome, pazienti.cognome
  							FROM persone AS medici
  							INNER JOIN persone AS pazienti on medici.id = pazienti.medico_id
  							WHERE medici.id = ?", array ($id));
    return $query->result();
  }

  public function get_medici()
  {
    $query = $this->db->query("SELECT *
      FROM persone
	  WHERE is_medico = 1
      ORDER BY id");
  	return $query->result();
  }
  public function delete_farmaco($id_persona, $id_farmaco)
  {
	  $rimuovi_farmaco = $this->db->query("DELETE FROM persone_has_farmaci WHERE persone_id = ? and farmaci_id = ?", array($id_persona, $id_farmaco));
	  //$rimuovi_farmaco->execute();
  }

  public function insert_farmaco($id_persona,$id_farmaco)
  {
	  $aggiungi_farmaco = $this->db->query("INSERT INTO persone_has_farmaci (persone_id, farmaci_id) VALUES (?, ?)", array($id_persona, $id_farmaco));
	  //$aggiungi_farmaco->execute();
  }

  public function registra_utente($nome, $cognome, $email, $cf, $residenza, $medico, $ruolo, $password, $sesso)
  {

	$id_medico = $medico;

	  if($ruolo == "Paziente")
	  {
		  $colonna_ruolo = "is_paziente";
	  }
	  else if($ruolo == "Medico")
	  {
		  $colonna_ruolo = "is_medico";
	  }
	  else if($ruolo == "Segretario")
	  {
		  $colonna_ruolo = "is_segretario";
	  }
	  else if($ruolo == "Amministratore")
	  {
		  $colonna_ruolo = "is_amministratore";
	  }
	  $password = md5($password);

	  $query = $this->db->query("INSERT INTO persone (nome, cognome, email, password, sesso, codice_fiscale, residenza, medico_id, ".$colonna_ruolo.")
								VALUES(?, ?, ?, ?, ?, ?, ?, ?, 1)", array($nome, $cognome, $email, $password, $sesso, $cf, $residenza, $id_medico));
  }
}
